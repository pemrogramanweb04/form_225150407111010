<!DOCTYPE html>
<html>
<head>
    <title>Formulir</title>
    <style>
        table {
            margin: auto;
            border-collapse: collapse;
        }
        td, th {
            padding: 8px;
            border: 1px solid black;
        }
        h2 {
            text-align: center;
        }
        input[type="submit"] {
            border: 1px solid black;
            padding: 5px 10px;
            cursor: pointer;
            border-radius: 5px;
        }
    </style>
</head>
<body>
    <h2>Formulir</h2>
    <form action="form2[225150407111010].php" method="post">
        <table>
            <tr>
                <td>Nama:</td>
                <td><input type="text" name="nama"></td>
            </tr>
            <tr>
                <td>Riwayat Sakit:</td>
                <td>
                    <input type="radio" name="riwayat_sakit" value="Ada"> Ada<br>
                    <input type="radio" name="riwayat_sakit" value="Tidak Ada"> Tidak Ada
                </td>
            </tr>
            <tr>
                <td>Tingkat Kesadaran:</td>
                <td>
                    <select name="tingkat_kesadaran">
                        <option value="" disabled selected>Pilih</option>
                        <option value="Compos Mentis">Compos Mentis</option>
                        <option value="Apatis">Apatis</option>
                        <option value="Delirium">Delirium</option>
                        <option value="Somnolen">Somnolen</option>
                        <option value="Soporous">Soporous</option>
                        <option value="Semikoma">Semikoma</option>
                        <option value="Koma">Koma</option>
                    </select>
                </td>
            </tr>
        </table>
        <br>
        <div style="text-align:center;">
            <input type="submit" value="Submit">
        </div>
    </form>
</body>
</html>